from django.views.generic import ListView

from .models import Post


class PostIndexView(ListView):
    model = Post
    template_name = 'posts/index.html'
    context_object_name = 'posts'