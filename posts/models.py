from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=128, verbose_name='Тема')
    summary = models.TextField(max_length=5000, verbose_name='Описание')
    image = models.ImageField(upload_to='post_images', verbose_name='Фото')
    date = models.CharField(max_length=120, verbose_name='Дата')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Posts'
        verbose_name_plural = 'Posts'