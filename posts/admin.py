from django.contrib import admin
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'summary']
    list_filter = ['title']
    list_display_links = ['title']
    search_fields = ['title']
    fields = ['title', 'summary', 'image', 'date']


admin.site.register(Post, PostAdmin)
