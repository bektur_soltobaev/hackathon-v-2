# High Technology Park
## Hackathon 

This project is for the upcoming Hackathon. Hackathon where all developer can create something better for future.
Sprint Parliament is a creative technology workshop where developers (IT specialists), designers and experts, trainers, educators in the field of civic education and parliamentarism will work on creating training programs, games and applications in a two-day sprint mode. 
The goal of the joint work will be the creation of prototypes of training services on parliamentarism.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Stack Technologies
1. [Django](https://www.djangoproject.com/)
2. [Python3+](https://www.python.org/download/releases/3.0/)
3. [HTTML&CSS](https://www.w3schools.com/html/html_css.asp)
4. [Virtualenv](https://docs.python-guide.org/dev/virtualenvs/)

### Installing

What things you need to install the software and how to install them.

+ Install [venv](https://realpython.com/python-virtual-environments-a-primer/) (feel free to give any name for your virutalenv)
+ Install requirement through commmand  ```pip install -r requiremments.txt```
+ Make migrations and migrate through command ```./manage.py makemigrations and ./manage.py migrate```

## Authors

* **Bektur Soltobaev** - *Initial work* - [GitLab](https://gitlab.com/Ever_lncr)
    * Backend developer

<small>See also the list of contributors who participated in this project.</small>

* *[Asel Sultanova](https://github.com/Grayhat00)*

  * Front-end developer

* *[Bektursun Samat uulu](https://gitlab.com/samatuulu)*

  * Backend developer